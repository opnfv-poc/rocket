=====================================================
vSwitch Hardware Acceleration Technology Requirements
=====================================================

-------------------
Before Introduction
-------------------

This standard proposes technical requirements for the functions, performance, reliability, and management of the NFV v Switch hardware acceleration network card to guide the application of the China Mobile NFV vSwitch hardware acceleration network card.

This standard mainly includes the technical requirements, functions, performance, reliability, scalability, manageability, security and other technical requirements of the NFV vSwitch hardware acceleration network card , and the interface requirements provided by the vSwitch hardware acceleration network card.

1.          Scope

This standard specifies the technical requirements for the functions, performance, reliability, management, etc. of China Mobile's NFV vSwitch accelerated network card products. It is used by China Mobile Group and provincial companies and is suitable for the selection, testing and use of NFV vSwitch accelerated network card products.

Unless specifically stated, within the meaning of this standard " servers " are x86 server architecture.
The meaning of " optional " in this document is the requirement that support is not necessary.

2.          Terms, definitions and abbreviations

The following terms, definitions and abbreviations apply to this standard:

3.          vSwitch Hardware Acceleration Overview

vSwitch unload function , the core by an FPGA (field programmable gate array) , a coprocessor or ASIC, etc. NIC hardware to assist the CPU processing load of the network, network programming interface function, having body has the following characteristics:

- Help the CPU to handle network load by programming to support data surface and control plane function customization
- Usually contains multiple ports and internal switches to quickly forward data
- Detect and manage network traffic based on network packets .

Accelerated NICs have the advantage of enabling application and virtualization performance, enabling software-defined networking (SDN) and network function virtualization (NFV), sinking vSwitch- related features to accelerated NICs , ensuring maximum processing power for applications.
The vSwitch accelerates the NIC to the northbound SDN controller and virtual layer, and connects to the hardware server .

4. Accelerate network card configuration requirements

5.  Functional requirement










5.1 Network Isolation and Address Multiplexing

Supports Layer 2 interworking of virtual machines in the same virtual network and Layer 2 isolation of VMs between different virtual networks.

Supports the same vSwitch and vNIC under different virtual networks to use the same IP address and MAC address.

5.2 MTU adjustable

Need to support the MTU value is set, the default is 4000B, the maximum supportable 90 54 is B .

5.3 Multicast

The need to support the right to forward multicast packets(For example, if the destination address is a multicast address packet,VLANInternal broadcast), and support unknown multicast (that is, multicast that is not registered in the system beforehand).You need to support the multicast forwarding table to query and select the corresponding port to send multicast packets. If the multicast forwarding table fails to be searched, the multicast packets are broadcast in the VLAN . At the same time, the vSwitch software part needs to support the analysis and learning of IGMP protocol packets, and deliver the learned multicast forwarding table to the vSwitch hardware acceleration network card .

5.4 promiscuous mode

Promiscuous modes that support vNIC, including unicast and multicast hybrids. That is, it is possible to correctly forward the data packet in which the source MAC of the VM is inconsistent with the MAC of the vNIC and the packet addressed to the MAC of the VM and the MAC of the vNIC.

Supports the self-learning function of promiscuous mode, records the MAC address of the forwarded data packet to the forwarding table, and forwards it according to the table in the subsequent forwarding process. The forwarding table update function automatically updates the forwarding table when the MAC address is migrated.

defaultClose promiscuous mode,And support by HAOpen.

5.5 port aggregation

The network port aggregation and vSwitch stacking functions of the NICs are supported . The aggregation mode supports active-backup and 802.3ad .

5.6           Support v NIC multi-queue

The vNIC multi-queue function needs to be supported , and the number of queues can be set.

5.7           Virtual machine migration

Accelerated NICvNICIt should support the virtual machine of hot and cold migration migration.

5.8           Virtual machine data access

You need to enable the vSwitch bridge function. The Layer 2 communication between the two vNICs on the vSwitch of the same acceleration network port does not require an egress port.

vSwitch accelerator card is capable virtual machine Virtio normal vNIC pass type channel , 64 may be provided Virtio vNIC type.

5.9 Network Protocol

Need to support ARP/GARP,ICMP/ICMPv6, GRE,VLAN,V xLANProtocols that support transparent transmission of OSPF, BGP, BFD, etc..

5.10         VLAN transparent transmission

Support forVLANType andVxLANType of virtual network provisioningVLANTransparent support.

1)         based onVLANType virtual networkVLANPenetrate

needstand byforVLANTypes of virtual network offerings are basedQinQofVLANTransparent transmission support, that is, from the inside of the virtual machineVLANThe tagged packets will be tagged with virtual networks.VLANPost forwardingTPIDNeed to be0x8100), while the tape is sent from inside the virtual machineVLANThe tagged packet will retain the originalVLANMark and add extra virtual network on the outer layerVLANPost forwardingTPIDNeed to be0x8100).

Need to support turning on or off for virtual networksVLANTransparent transmission, that is, when creating a virtual network--VLAN-transparentThe parameter determines whether to turn it on or offVLANTransparent transmission, if this parameter is not specified, it needs to be closed by default.VLANPenetrate.

2)         based on VxLANType virtual networkVLAN Penetrate

needstand byforVxLANTypes of virtual network offerings are basedVLAN-aware-vmsVLANTransparent support,based onVLAN-aware-vms TrunkportwithSUbPortSupportVxLANType of virtual network, andSubportSupport for setting the segment type (needstand byVLAN) and Segment ID. Trunk on the same trunkPortAnd multiple Subports need to support the same MAC configured.

TrunkportAnd SubPortAllocate corresponding virtual machine switchesPort, where Trunkport will be virtual machine as vNICuse.Unsigned from inside the virtual machineVLANMarked packets willConverted to the virtual network where the Trunkport is locatedofVxLANPacket forwarding,andBand from inside the virtual machineVLANTagged packetWill be converted to Segment IDWith data packetsVLAN IDEqual Subport is in the virtual networkVxLANPacket forwarding.

5.11 supports IPv4 and IPv6

VxLAN services are supported on both IPv4 and IPv6 networks (both IPv4 and IPv6, and both). VTEP supports both IPv4 and IPv6.

5.12 QoS

Requires support for bandwidth-based bidirectional QoS.

Supports port is the maximum limit bandwidths, support Port Maximum burst bandwidth to exceed the maximum limit flow bandwidth discards (instantaneous flow rate exceeds the maximum allowed limit but more than the bandwidth does not exceed the maximum burst bandwidth) .

5.13        Traffic statistics and reporting

Supports port-based traffic statistics, which can be used to classify statistics on unicast, multicast, and broadcast packets. Supports statistics based on protocols and flows, discards packet classification statistics, and error packet statistics .

Supports port mirroring to mirror traffic to vNICs or physical ports without affecting services . It supports mirroring ingress, egress, and both directions. It also mirrors the traffic of multiple ports to the same port.

5.14        SDN compatibility requirements

1. the vSwitch need to support OpenFlow protocol and OVS DB Configuration Protocol, by receiving SDN controller delivered by the configuration and flow tables, different services of VLAN isolation, two / three service forwarding, network QoS (the QoS such as bandwidth-based), Functions such as virtual machine traffic statistics and port mirroring .

2. Support for supporting OVSDB to complete virtualized bridge configuration .

3. The vSwitch needs to support the active mode of the SDN controller, that is, the SDN controller initiates an OVS DB connection request, and the virtual switch answers. Wherein SDN controller initiates a connection request object IP as VIM or Hypervisor management host IP , port number of the virtual switch can be set, before making a request, the SDN the controller pre-configured virtual switch where the host IP and port information .

4. The vSwitch that the hardware is uninstalled needs to support the distributed routing function and meet the requirements of the China Mobile SDN Controller Southbound Interface Specification - Virtual Switch Volume .

5. A CLI is required and support for configuring flow table and port information through the command line .

6. vSwitch hardware acceleration does not depend on the server platform

5.15        C yborg (optional)

Support Cyborg 's flexible and unified management of accelerated NIC hardware and software acceleration devices.
